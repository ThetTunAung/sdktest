package com.haulio.mylibrary

import android.content.Context
import android.content.Intent
import android.view.View

class MyLibrary{
    companion object{
        fun init(context : Context){
            LibraryButton.setCustomClickListener(object : LibraryButton.CustomOnClickListener{
                override fun onClick(var1: View?) {
                    context.startActivity(Intent(context,LibraryMainActivity::class.java))
                }
            })
        }
    }
}