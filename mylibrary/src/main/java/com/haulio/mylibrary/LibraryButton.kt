package com.haulio.mylibrary
import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatButton


class LibraryButton : AppCompatButton, View.OnClickListener {
    private var mContext: Context? = null

    companion object{
        private var event: CustomOnClickListener? = null
        fun setCustomClickListener(event: CustomOnClickListener?) {
            this.event = event
        }
    }

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        mContext = context
        setOnClickListener(this)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    override fun onClick(v: View?) {
        event!!.onClick(v)
    }

    interface CustomOnClickListener {
        fun onClick(var1: View?)
    }
}