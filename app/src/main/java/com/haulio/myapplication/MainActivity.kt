package com.haulio.myapplication

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.haulio.mylibrary.LibraryMainActivity
import com.haulio.mylibrary.MyLibrary
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MyLibrary.init(this)
        /*btnLaunch.setOnClickListener {
            startActivity(Intent(this@MainActivity, LibraryMainActivity::class.java))
        }*/
    }
}
